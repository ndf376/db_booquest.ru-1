<?php
$path = pathinfo(__FILE__) ['dirname'];

include ($path . '/config.php');

?>

<span class="mk-tooltip <?php echo $el_class; ?> js-el" data-mk-component="Tooltip">
	<a href="<?php echo $href; ?>" class="mk-tooltip--link"><?php echo $text; ?></a>
	<span class="mk-tooltip--text"><?php echo $tooltip_text; ?></span>
</span>
