<h3 class="mk-fancy-title pattern-style">
	<span>  Widget Title </span>
</h3>


<div class="mk-news-tab  js-el" data-mk-component="Tabs">

	<div class="mk-news-tab-heading">
		<span class="mk-news-tab-title"> Tab Title </span>
		<ul class="mk-tabs-tabs">

				<li class="mk-tabs-tab"><a href="#"> Tab Name </a></li>

			<!-- <div class="clearboth"></div> -->
		</ul>
		<div class="clearboth"></div>
	</div>

	<div class="mk-tabs-panes page-bg-color">
		<div class="mk-tabs-pane">
			<div class="title-mobile"> Tab Name </div>

			<div class="news-tab-wrapper left-side">
				<a href="" class="news-tab-thumb">
					<img alt="" title="" src="" />
				</a>

				<h3 class="the-title"><a href=""> The Title </a></h3>
				<div class="the-excerpt"> The Excerpt </div>
				<a class="new-tab-readmore" href="">
					<span>Read more</span>
					<i class="mk-icon-caret-right"></i>
				</a>
			</div>

			<div class="news-tab-wrapper right-side">
				<h3 class="the-title"><a href=""> The Title </a></h3>
				<div class="the-excerpt"> The Excerpt </div>
				<a class="new-tab-readmore" href="">
					<span>Read more</span>
					<i class="mk-icon-caret-right"></i>
				</a>
			</div>

			<!-- <div class="clearboth"></div> -->
		</div>
	</div>
</div>

