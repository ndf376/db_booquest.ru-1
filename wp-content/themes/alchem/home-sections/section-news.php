<?php 
  // section news 
  
   global $allowedposttags;
   $section_hide    = absint(alchem_option('section_8_hide',0));
   $content_model   = absint(alchem_option('section_8_model',0));
   $section_id      = esc_attr(sanitize_title(alchem_option('section_8_id','section-9')));
   $section_title   = wp_kses(alchem_option('section_8_title'), $allowedposttags);
   $section_content = wp_kses(alchem_option('section_8_content'), $allowedposttags);
   $columns         = absint(alchem_option('section_8_columns',3));
   $col             = $columns>0?12/$columns:4;
   $posts_num       = absint(alchem_option('section_8_posts_num',3));
 ?> 
 <?php if( $section_hide != '1' ):?> 
 <section class="section magee-section parallax-scrolling alchem-home-section-8" id="<?php echo $section_id;?>">
  <div class="section-content container">
  <?php if( $content_model == 0 ):?>
  <?php if( $section_title != '' ):?>
    <h2 style="text-align: center"><?php echo $section_title;?></h2>
    <div class="divider divider-border center" style="margin-top: 30px;margin-bottom:50px;width:80px;">
      <div class="divider-inner divider-border" style="border-bottom-width:3px; border-color:#fdd200;"></div>
    </div>
    <?php endif;?>
    <div style="color:#fff;">
     <?php
	 $news_item   = '';
	 $news_str    = '';
	 $j           = 0;
	 query_posts( 'ignore_sticky_posts=1&posts_per_page='.$posts_num );

// The Loop
while ( have_posts() ) : the_post();

$thumb = '';
if ( has_post_thumbnail() ) { 
   $thumb  = get_the_post_thumbnail( get_the_ID(), 'alchem-portfolio-thumb' );
}
   
   $news_item .= '<div class=" col-md-'.$col.'">
  <div class="col-sm-12"><a href="'.get_permalink().'" rel="attachment wp-att-3544">'.$thumb.'</a></div>
  <div class="col-sm-12">
    <h2 style="text-align: center; font-size: 20px; text-transform: none; color: #464f5d;"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
    <p style="color: #464f5d; font-size: 14px;">'.get_the_excerpt().'</p>
  </div>
  <div class="clear"></div>
</div>';
       $m = $j+1;
	  if( $m % $columns == 0 ){
	        $news_str .= '<div class="row">'.$news_item.'</div>';
	        $news_item   = '';
	   }
 $j++;
endwhile;

if( $news_item != '' ){
		    $news_str .= '<div class="row">'.$news_item.'</div>';
	      
		   }
// Reset Query
 wp_reset_query();
 echo $news_str;	 

	  ?>      
    </div>
    <?php else:?>
 <?php echo do_shortcode($section_content);?>
 <?php endif;?>
  </div>
</section>
<?php endif;?>